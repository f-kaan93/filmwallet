import {observer} from 'mobx-react'
import React from 'react'
import { Animated, Easing, Text, TextStyle, ViewStyle} from 'react-native'
import colors from '../colors'
import store from '../model/store'
import Icon from 'react-native-vector-icons/Ionicons'

@observer export default class Error extends React.Component {
  state = {
    opacity: new Animated.Value(0),
  }
  showError = () => {
    Animated.sequence([
      Animated.timing(this.state.opacity, {
        duration: 300,
        easing: Easing.out(Easing.cubic),
        toValue: 1,
      }),
      Animated.timing(this.state.opacity, {
        duration: 300,
        delay: 4000,
        easing: Easing.out(Easing.cubic),
        toValue: 0,
      }),
    ]).start(() => {
      store.error.visible = false
      store.error.message = ''
    })
  }
  render() {
    const container: ViewStyle = {
      position: 'absolute',
      zIndex: 200,
      bottom: 70,
      right: 10,
      left: 10,
      height: 40,
      borderRadius: 20,
      alignItems: 'center',
      flexDirection: 'row',
      paddingHorizontal: 20,
      backgroundColor: colors.highlight,
    }
    const txt: TextStyle = {
      color: colors.text,
      marginLeft: 5,
      fontSize: 18,
    }
    if (store.error.visible) {
      this.showError()
    }
    return store.error.visible && (
      <Animated.View style={{ ...container, opacity: this.state.opacity }}>
        <Icon name='ios-alert' color={ colors.text } size={ 25 }/>
        <Text style={ txt }>
          { store.error.message }
        </Text>
      </Animated.View>
    )
  }
}
