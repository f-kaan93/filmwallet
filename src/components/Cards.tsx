import React, { ReactNodeArray } from 'react'
import {
  Animated,
  Easing,
  PanResponder,
  PanResponderGestureState,
  Text,
  TextStyle,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native'

const panReleaseThresholdX = 130
const panReleaseThresholdy = 150
const dragStartThreshold = 30

type props = {
  children: ReactNodeArray,
  loop?: boolean,
  onSwipeLeftMessage?: string,
  onSwipeRightMessage?: string,
  onSwipeUpMessage?: string,
  messageStyle?: TextStyle,
  onPress?: (index: number) => void,
  onDone?: () => void,
  onIndex?: (index: number) => void,
  onSwipeLeft: (index: number) => void,
  onSwipeRight: (index: number) => void,
  onSwipeUp: (index: number) => void,
}

class Cards extends React.Component<props, {}> {
  state = {
    pan: new Animated.ValueXY(),
    opacity: new Animated.Value(1),
    cardIndex: 0,
  }
  panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => false,
    onMoveShouldSetPanResponder: (evt, gestureState) => this.isDragged(gestureState),
    onPanResponderMove: Animated.event([
      null, { dx: this.state.pan.x, dy: this.state.pan.y },
    ]),
    onPanResponderRelease: (evt, gestureState) => this.onRelease(gestureState),
  })

  isDragged = ({ dx, dy }: { dx: number, dy: number }) => {
    const draggedHorizontal = Math.abs(dx) > dragStartThreshold
    const draggedVertical = Math.abs(dy) > dragStartThreshold
    const dragged = draggedHorizontal || draggedVertical
    return dragged
  }
  onRelease = ( gestureState: PanResponderGestureState ) => {
    const {vx, vy} = gestureState
    const pan: any = this.state.pan

    const decayAnim = Animated.decay(pan, {
      velocity: { x: vx, y: vy },
      deceleration: 0.98,
    })
    if (pan.y.__getValue() * -1 > panReleaseThresholdy) {
      this.props.onSwipeUp(this.state.cardIndex)
      decayAnim.start(this.nextCard)
    } else if (pan.x.__getValue() > panReleaseThresholdX) {
      this.props.onSwipeRight(this.state.cardIndex)
      decayAnim.start(this.nextCard)
    } else if (pan.x.__getValue() * -1 > panReleaseThresholdX) {
      this.props.onSwipeLeft(this.state.cardIndex)
      decayAnim.start(this.nextCard)
    } else {
      Animated.spring(pan, {
        toValue: { x: 0, y: 0 },
        friction: 4,
      }).start()
    }
  }
  onPress = () => {
    if (this.props.onPress) {
      this.props.onPress(this.state.cardIndex)
    }
  }
  nextCard = () => {
    let cardIndex = this.state.cardIndex + 1
    if (cardIndex === this.props.children.length) {
      if (this.props.loop) {
        cardIndex = 0
      } else {
        if (this.props.onDone) {
          this.props.onDone()
        }
      }
    }
    Animated.timing(this.state.opacity, { toValue: 0, duration: 100 }).start(() => {
      this.setState({ cardIndex }, () => {
        this.state.pan.setValue({ x: 0, y: 0 })
        Animated.timing(this.state.opacity, {
          toValue: 1,
          easing: Easing.out(Easing.circle),
          duration: 600,
        }).start()
      })
    })
  }
  onIndex() {
    if (this.props.onIndex) {
      this.props.onIndex(this.state.cardIndex)
    }
  }
  render() {
    const { pan } = this.state
    const [translateX, translateY] = [pan.x, pan.y]
    const rotate = pan.x.interpolate({ inputRange: [-200, 0, 200], outputRange: ['-30deg', '0deg', '30deg'] })
    const card = {
      transform: [{ translateX }, { translateY }, { rotate }],
      opacity: this.state.opacity,
    }
    const upMessage = {
      position: 'absolute',
      opacity: pan.y.interpolate({
        inputRange: [(panReleaseThresholdy + 100) * -1, panReleaseThresholdy * -1],
         outputRange: [1, 0],
        }),
    }
    // Multiplying with zero if pan is in the swipe up range so that they don't show
    const leftMessage = {
      position: 'absolute',
      opacity: Animated.multiply(
        pan.x.interpolate({
          inputRange: [(panReleaseThresholdX + 100) * -1, panReleaseThresholdX * -1],
          outputRange: [1, 0],
        }),
        pan.y.interpolate({
          inputRange: [(panReleaseThresholdy + 1) * -1, panReleaseThresholdy * -1, (panReleaseThresholdy - 1) * -1, 0],
          outputRange: [0, 0, 1, 1],
          },
        ),
      ),
    }
    const rightMessage = {
      position: 'absolute',
      opacity: Animated.multiply(
        pan.x.interpolate({
          inputRange: [panReleaseThresholdX, panReleaseThresholdX + 100],
          outputRange: [0, 1],
        }),
        pan.y.interpolate({
          inputRange: [(panReleaseThresholdy + 1) * -1, panReleaseThresholdy * -1, (panReleaseThresholdy - 1) * -1, 0],
          outputRange: [0, 0, 1, 1] },
        ),
      ),
    }
    const messageContainer: ViewStyle = {
      alignItems: 'center',
      position: 'relative',
    }

    const text: TextStyle = this.props.messageStyle || {
      fontSize: 20,
    }
    this.onIndex()
    return (
      <View>
        {/* Caching next few items */}
        <View style={{ position: 'absolute', width: 0, height: 0, opacity: 0 }}>
          {this.props.children[this.state.cardIndex + 1]}
          {this.props.children[this.state.cardIndex + 2]}
          {this.props.children[this.state.cardIndex + 3]}
        </View>
        <Animated.View style={card} {...this.panResponder.panHandlers}>
          <TouchableWithoutFeedback onPress={this.onPress}>
            {this.props.children[this.state.cardIndex]}
          </TouchableWithoutFeedback>
        </Animated.View >
        <View style={messageContainer}>
          <Animated.View style={upMessage}>
            <Text style={text}>
              {this.props.onSwipeUpMessage}
            </Text>
          </Animated.View>
          <Animated.View style={leftMessage}>
            <Text style={text}>
              {this.props.onSwipeLeftMessage}
            </Text>
          </Animated.View>
          <Animated.View style={rightMessage}>
            <Text style={text}>
              {this.props.onSwipeRightMessage}
            </Text>
          </Animated.View>
        </View>
      </View>
    )
  }
}

export default Cards
