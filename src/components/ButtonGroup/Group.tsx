import React from 'react'
import { View, ViewStyle } from 'react-native'
import colors from '../../colors'
import Button from './Button'

type props = {
  onPress: (index: number) => void,
  selectedIndex: number,
  buttons: string[],
}

export default class Group extends React.Component<props, {}> {
  render() {
    const container: ViewStyle = {
      alignItems: 'center',
      paddingBottom: 10,
    }
    const group: ViewStyle = {
      borderRadius: 4,
      borderWidth: 1,
      borderColor: colors.highlight,
      overflow: 'hidden',
      flexDirection: 'row',
    }
    return (
      <View style={container}>
        <View style={group}>
          {this.props.buttons.map((button, index) => (
            <Button
              onPress={() => this.props.onPress(index)}
              key={index}
              selected={index === this.props.selectedIndex}
              title={button}
              borderLeft={index > 0} />
          ))}
        </View>
      </View>

    )
  }
}
