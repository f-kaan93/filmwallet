import React from 'react'
import {
  Animated,
  Easing,
  TouchableWithoutFeedback,
} from 'react-native'
import colors from '../../colors'

type props = {
  title: string,
  onPress: () => void,
  selected: boolean,
  borderLeft: boolean,
}

export default class Button extends React.Component<props, {}> {
  state = {
    selected: new Animated.Value(this.props.selected ? 1 : 0),
  }
  initAnim = () => {
    Animated.timing(this.state.selected, {
      duration: 300,
      easing: Easing.out(Easing.cubic),
      toValue: this.props.selected ? 1 : 0,
    }).start()
  }
  render() {
    this.initAnim()
    const backgroundColor = this.state.selected.interpolate({
      inputRange: [0, 1],
      outputRange: ['transparent', colors.highlight],
    })
    const color = this.state.selected.interpolate({
      inputRange: [0, 1],
      outputRange: [colors.highlight, colors.text],
    })
    const buttonContainer = {
      paddingHorizontal: 14,
      paddingVertical: 9,
      borderLeftWidth: this.props.borderLeft ? 1 : 0,
      borderLeftColor: colors.highlight,
      backgroundColor,
    }
    const text = {
      color,
    }
    return (
      <TouchableWithoutFeedback onPress={this.props.onPress}>
        <Animated.View style={buttonContainer}>
          <Animated.Text style={text}>
            {this.props.title}
          </Animated.Text>
        </Animated.View>
      </TouchableWithoutFeedback>
    )
  }
}
