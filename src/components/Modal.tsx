import { BlurView } from 'expo'
import React from 'react'
import { Dimensions, Modal, View, ViewStyle } from 'react-native'
import colors from '../colors'

type props = {
  visible: boolean,
  onRequestClose: () => void,
}

export default class CustomModal extends React.Component<props, {}> {
  render() {
    const overlay: ViewStyle = {
      position: 'absolute',
      width: Dimensions.get('screen').width,
      height: Dimensions.get('screen').height,
      zIndex: 100,
      top: 0,
      left: 0,
      justifyContent: 'center',
      alignItems: 'stretch',
      paddingVertical: 100,
      paddingHorizontal: 10,
    }
    const container: ViewStyle = {
      backgroundColor: colors.popUpBackground,
      flex: 1,
      borderRadius: 15,
    }
    return (
      <Modal
        onRequestClose={this.props.onRequestClose}
        animationType='fade'
        transparent={true}
        visible={this.props.visible}>
        <BlurView tint='dark' intensity={90} style={overlay}>
          <View style={container}>
            {this.props.children}
          </View>
        </BlurView>
      </Modal>
    )
  }
}
