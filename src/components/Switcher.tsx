import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import colors from '../colors'

type props = {
  buttonNames: string[],
  activeIndex: number,
  onPress: (index: number) => void,
}

export default class Switcher extends React.Component<props, {}> {
  render() {
    return (
      <View style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%',
      }}>
        {this.props.buttonNames.map((name, index) => (
          <TouchableOpacity
            key={index}
            onPress={() => {
              this.props.onPress(index)
            }}
            style={{
              borderBottomColor: this.props.activeIndex === index ? colors.highlight : 'transparent',
              borderBottomWidth: 2,
              padding: 3,
            }}>
            <Text
              style={{
                fontSize: 18,
                color: colors.text,
              }}>
              {name}
            </Text>
          </TouchableOpacity>
        ))}

      </View >
    )
  }
}
