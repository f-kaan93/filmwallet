import { LinearGradient } from 'expo'
import React from 'react'
import { Dimensions } from 'react-native'
import colors from '../colors'

export default class Background extends React.Component {
  render() {
    return (
      <LinearGradient
        colors={colors.appBackground}
        style={{
          position: 'absolute',
          left: 0,
          top: 0,
          width: Dimensions.get('screen').width,
          height: Dimensions.get('screen').height,
        }}
        start={[0, 0]} />
    )
  }
}
