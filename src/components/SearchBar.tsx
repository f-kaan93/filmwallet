import { observer } from 'mobx-react'
import React from 'react'
import { Animated, Easing, TextInput, TextStyle, View, ViewStyle } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import colors from '../colors'

type state = {
  input: string,
  opacity: Animated.Value,
}
type props = {
  onSearch: (input: string) => void,
  onCancel: () => void,
  placeholder?: string,
}

@observer export default class SearchBar extends React.Component<props, state> {

  intervalHandler: any = undefined

  state = {
    input: '',
    opacity: new Animated.Value(0),
  }

  onChangeText = (input: string) => {
    this.setState({ input })
    clearInterval(this.intervalHandler)

    if (!input) {
      this.hideCancelIcon()
      this.props.onCancel()
      return
    }
    this.showCancelIcon()
    this.intervalHandler = setTimeout(() => {
      this.props.onSearch(input)
    }, 700)
  }

  showCancelIcon = () => {
    Animated.timing(this.state.opacity, {
      duration: 300,
      easing: Easing.in(Easing.cubic),
      toValue: 1,
    }).start()
  }

  hideCancelIcon = () => {
    Animated.timing(this.state.opacity, {
      duration: 300,
      easing: Easing.in(Easing.cubic),
      toValue: 0,
    }).start()
  }

  render() {
    const container: ViewStyle = {
      alignItems: 'center',
      backgroundColor: colors.elBackground,
      borderRadius: 30,
      flexDirection: 'row',
      marginLeft: 10,
      marginRight: 10,
      paddingBottom: 3,
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 3,
    }
    const input: TextStyle = {
      flex: 1,
      paddingBottom: 5,
      paddingTop: 5,
      color: colors.text,
    }
    // Not applying ViewStyle type because Animated Values are not supported
    const searchIconContainer = {
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: -2,
      opacity: this.state.opacity,
      paddingLeft: 5,
      paddingRight: 8,
    }
    return (
      <View style={container}>
        <View style={{
          paddingLeft: 5,
          paddingRight: 8,
        }}>
          <Icon
            name='ios-search'
            color={colors.text}
            size={16} />
        </View>
        <TextInput
          style={input}
          placeholder={this.props.placeholder || 'Search'}
          placeholderTextColor={colors.text}
          keyboardAppearance='dark'
          returnKeyType='done'
          onChangeText={this.onChangeText}
          value={this.state.input}
        />
        <Animated.View style={searchIconContainer}>
          <Icon
            name='ios-close-circle'
            color={colors.text}
            size={24}
            onPress={() => {
              this.onChangeText('')
            }} />
        </Animated.View>
      </View>

    )
  }
}
