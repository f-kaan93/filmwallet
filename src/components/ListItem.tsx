import React, { ReactElement } from 'react'
import {
  Image,
  ImageSourcePropType,
  ImageStyle,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native'
import colors from '../colors'

const margin = 10 // margin between left title and right

type props = {
  onPress?: () => void,
  avatar?: ImageSourcePropType,
  leftElement?: ReactElement<any>,
  rightElement?: ReactElement<any>,
  title: string,
  subtitle?: string,
  underline?: boolean,
}

export default class Background extends React.Component<props, {}> {
  renderTouchable = (children: ReactElement<any>) => {
    if (this.props.onPress) {
      return (
        <TouchableOpacity onPress={this.props.onPress}>
          {children}
        </TouchableOpacity>
      )
    }
    return (
      <View>
        {children}
      </View>
    )
  }
  renderLeft = () => {
    const { avatar, leftElement } = this.props
    if (avatar) {
      const img: ImageStyle = {
        width: 48,
        height: 48,
        borderRadius: 24,
        marginRight: margin,
      }
      return <Image source={avatar} style={img} />
    }
    if (leftElement) {
      return (
        <View style={{ marginRight: margin }}>
          {leftElement}
        </View>
      )
    }
    return null
  }
  renderRight = () => {
    const container: ViewStyle = {
      marginLeft: margin,
      justifyContent: 'center',
    }
    if (this.props.rightElement) {
      return (
        <View style={container}>
          {this.props.rightElement}
        </View>
      )
    }
    return null
  }
  renderListItem = () => {
    const container: ViewStyle = {
      flexGrow: 1,
      borderRadius: 15,
      marginVertical: 10,
      paddingVertical: 10,
      paddingHorizontal: 15,
      backgroundColor: colors.elBackground,
      flexDirection: 'row',
    }
    const underlinedContainer: ViewStyle = {
      flexGrow: 1,
      borderBottomWidth: 1,
      borderBottomColor: colors.highlight,
      marginVertical: 10,
      paddingVertical: 10,
      paddingHorizontal: 15,
      flexDirection: 'row',
    }
    const titleContainer: ViewStyle = {
      justifyContent: 'center',
      flex: 1,
    }
    const title: TextStyle = {
      color: colors.text,
      fontSize: 18,
      fontWeight: this.props.subtitle ? '500' : 'normal',
      marginBottom: this.props.subtitle ? 5 : 0,
    }
    const subtitle: TextStyle = {
      color: colors.text,
      fontSize: 14,
    }
    return (
      <View style={this.props.underline ? underlinedContainer : container}>
        {this.renderLeft()}
        <View style={titleContainer}>
          <Text style={title}>{this.props.title}</Text>
          {this.props.subtitle && <Text style={subtitle}>{this.props.subtitle}</Text>}
        </View>
        {this.renderRight()}
      </View>
    )
  }
  render() {
    const wrapper: ViewStyle = {
      width: '100%',
      paddingHorizontal: 10,
    }

    return (
      <View style={wrapper}>
        {this.renderTouchable(this.renderListItem())}
      </View>
    )
  }
}
