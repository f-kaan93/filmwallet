import { BlurView } from 'expo'
import React from 'react'
import { Text, TouchableHighlight, View } from 'react-native'
import { TabBarBottomProps } from 'react-navigation'

const TabBar = (props: TabBarBottomProps) => {
  return (
    <View>
      <BlurView
        tint='dark'
        intensity={90}
        style={{
          height: 50,
          width: '100%',
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 0,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
        }}>
        {
          props.navigation.state.routes.map((route, index) => {
            const {
              activeTintColor,
              inactiveTintColor,
              navigation,
              renderIcon,
            } = props
            const currentIndex = navigation.state.index
            const color = currentIndex === index ? activeTintColor : inactiveTintColor
            return (
              <TouchableHighlight
                key={index}
                onPress={() => { navigation.navigate({ routeName: route.routeName }) }}>
                <View style={{ alignItems: 'center' }}>
                  {renderIcon({ index, route, focused: currentIndex === index, tintColor: color })}
                  <Text style={{ color, fontSize: 10 }}>
                    {route.routeName}
                  </Text>
                </View>
              </TouchableHighlight>)
          })

        }
      </BlurView>
    </View>
  )
}

export default TabBar
