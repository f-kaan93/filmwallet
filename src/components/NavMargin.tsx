import React from 'react'
import { View } from 'react-native'

// When header is transparent, its position seems to set to absolute
// and it doesn't preserve its space, use this to add some margin.

export default class NavMargin extends React.Component {
  render() {
    return (
      <View style={{height: 70, width: '100%'}}></View>
    )
  }
}
