import React from 'react'
import { ActivityIndicator, View } from 'react-native'
import colors from '../colors'

export default class Loading extends React.Component {
  render() {
    return (
      <View style={{
        width: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <ActivityIndicator size='large' color={colors.highlight} />
      </View>
    )
  }
}
