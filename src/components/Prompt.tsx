import { BlurView } from 'expo'
import React from 'react'
import { Dimensions, Modal, StyleSheet, Text, TextInput, View } from 'react-native'
import colors from '../colors'
import Button from './Button'

type props = {
  title: string,
  message?: string,
  visible: boolean,
  input?: boolean,
  submitText?: string,
  cancelText?: string,
  placeholder?: string,
  onSubmit(input: string): void,
  onCancel(): void,
}

export default class Prompt extends React.Component<props, {}> {
  state = {
    input: '',
  }
  render() {
    return (
      <Modal
        onRequestClose={this.props.onCancel}
        transparent={true}
        visible={this.props.visible}
        animationType='fade'>
        <BlurView tint='dark' intensity={90} style={styles.overlay}>
          <View style={styles.box}>
            <View style={styles.messageContainer}>
              <Text style={styles.title}>
                {this.props.title}
              </Text>
              {!this.props.message ? null : (
                <Text style={styles.message}>
                  {this.props.message}
                </Text>
              )}
            </View>
            {!this.props.input ? null : (
              <View style={styles.inputContainer}>
                <View style={styles.inputBackground}>
                  <TextInput
                    style={styles.input}
                    placeholderTextColor={colors.text}
                    keyboardAppearance='dark'
                    placeholder={this.props.placeholder}
                    value={this.state.input}
                    onChangeText={(input) => { this.setState({ input }) }} />
                </View>
              </View>
            )}
            <View style={styles.buttonContainer}>
              <View style={styles.cancelButton}>
                <Button
                  color={colors.highlight}
                  title={this.props.cancelText || 'Cancel'}
                  onPress={() => {
                    this.props.onCancel()
                    this.setState({ input: '' })
                  }} />
              </View>
              <View style={styles.submitButton}>
                <Button
                  color={colors.highlight}
                  title={this.props.submitText || 'Submit'}
                  onPress={() => {
                    this.props.onSubmit(this.state.input)
                    this.setState({ input: '' })
                  }} />
              </View>
            </View>
          </View>
        </BlurView>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
    zIndex: 100,
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    width: 300,
    backgroundColor: colors.elBackground,
    alignItems: 'stretch',
    borderRadius: 15,
  },
  messageContainer: {
    padding: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontWeight: '600',
    fontSize: 17,
    color: colors.text,
  },
  message: {
    paddingTop: 10,
    color: colors.text,
  },
  inputContainer: {
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputBackground: {
    width: '100%',
    padding: 10,
    backgroundColor: colors.elBackground,
    borderRadius: 15,
  },
  input: {
    color: colors.text,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  cancelButton: {
    justifyContent: 'center',
    alignItems: 'center',
    flexGrow: 1,
    paddingTop: 5,
    paddingBottom: 5,
  },
  submitButton: {
    justifyContent: 'center',
    alignItems: 'center',
    flexGrow: 1,
    paddingTop: 5,
    paddingBottom: 5,
  },
})
