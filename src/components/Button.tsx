import React from 'react'
import { Text, TextStyle, TouchableOpacity, ViewStyle } from 'react-native'

type props = {
  onPress: () => void,
  title: string,
  color?: string,
  underline?: true,
  fontSize?: number,
}

export default class Button extends React.Component<props, {}> {
  render() {
    const txt: TextStyle = {
      color: this.props.color || 'blue',
      margin: 8,
      fontSize: this.props.fontSize || 18,
    }
    const underline: ViewStyle = {
      borderBottomWidth: 1,
      borderBottomColor: this.props.color || 'blue',
    }
    return (
      <TouchableOpacity style={this.props.underline && underline} onPress={this.props.onPress}>
        <Text style={txt}>
          {this.props.title}
        </Text>
      </TouchableOpacity>
    )
  }
}
