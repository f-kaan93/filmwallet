import axios from 'axios'
import { detailedDiscoverOptions, movie, movieList, personList } from './types'

const apiKey = 'b15bd053235ccdd5e3d2a4d8130bb71d'

export const searchMovie = (query: string) => {
  const url = 'https://api.themoviedb.org/3/search/movie'
  const combined = `${url}?api_key=${apiKey}&query=${query}`
  return axios.get(combined).then((response) => (
    response.data as movieList
  ))
}

export const searchActor = (query: string, page: number) => {
  const url = `https://api.themoviedb.org/3/search/person?api_key=${apiKey}&query=${query}&page=${page}`
  return axios.get(url).then((res) => (res.data as personList))
}

export const getMovie = (id: number) => {
  const url = 'https://api.themoviedb.org/3/movie/'
  const combined = `${url}${id}?api_key=${apiKey}`
  return axios.get(combined).then((response: { data: movie }) => response.data)
}

export const getMovieDetails = (id: number) => {
  const url = 'https://api.themoviedb.org/3/movie/'
  const basic = axios.get(`${url}${id}?api_key=${apiKey}`)
  const credits = axios.get(`${url}${id}/credits?api_key=${apiKey}`)
  const images = axios.get(`${url}${id}/images?api_key=${apiKey}`)
  const videos = axios.get(`${url}${id}/videos?api_key=${apiKey}`)

  return Promise.all([basic, credits, images, videos]).then((response) => {
    const combined = {
      ...response[0].data,
      ...response[1].data,
      ...response[2].data,
      videos: response[3].data.results,
    }
    return combined
  })
}

// Imdb top rated, max 7000 movies or so.
export const getTopRatedMovies = (page: number) => {
  const url = 'https://api.themoviedb.org/3/movie/top_rated?' +
    `api_key=${apiKey}&language=en-US&page=${page}`
  return axios.get(url).then((res) => (res.data as movieList))
}

export const getRelatedMovies = (id: number) => {
  const url = `https://api.themoviedb.org/3/movie/${id}/recommendations?api_key=${apiKey}`
  return axios.get(url).then((res) => res.data as movieList)
}

export const discoverDetailed = (details: detailedDiscoverOptions, page: number) => {
  const baseUrl = 'https://api.themoviedb.org/3/discover/movie?'
  const apiQuery = `api_key=${apiKey}`
  const yearQuery = !details.year.year ? '' : (
    details.year.specific ?
      `primary_release_year=${details.year.year}` :
      `primary_release_date.gte=${details.year.year}&primary_release_date.lte=${details.year.year + 9}`
  )
  const withGenresQuery = details.genres.withGenres.length === 0 ? '' : (
    `with_genres=${details.genres.withGenres.join(details.genres.combined ? ',' : '|')}`
  )
  const withoutGenresQuery = details.genres.withoutGenres.length === 0 ? '' : (
    `without_genres=${details.genres.withoutGenres.join(',')}`
  )
  const castQuery = details.actors.length === 0 ? '' : (
    `with_cast=${details.actors.map((actor) => (actor.id)).join(',')}`
  )
  const runtimeQuery = !details.runtime.runtime ? '' : (
    `with_runtime.${details.runtime.longerThan ? 'gte' : 'lte'}=${details.runtime.runtime}`
  )
  const pageQuery = `page=${page}`
  const filteringAndSortingQuery = 'vote_count.gte=20&sort_by=vote_average.desc'
  const url = [
    baseUrl,
    apiQuery,
    yearQuery,
    withGenresQuery,
    withoutGenresQuery,
    castQuery,
    runtimeQuery,
    filteringAndSortingQuery,
    pageQuery,
  ]
    // Remove unused queries which are empty strings
    .filter((q) => !!q)
    .join('&')
  return axios.get(url).then((res) => (res.data as movieList))
}
