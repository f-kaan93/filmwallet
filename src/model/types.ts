export type list = {
  name: string,
  movies: movie[],
  type: 'default' | 'custom',
}

export type wallet = list[]

export type basicMovie = {
  vote_count: number,
  id: number,
  video: boolean,
  vote_average: number,
  title: string,
  popularity: number,
  poster_path: string,
  original_language: string,
  original_title: string,
  genre_ids: number[],
  backdrop_path: string,
  adult: boolean,
  overview: string,
  release_date: string,
}

export type movie = {
  adult: boolean,
  backdrop_path: string,
  belongs_to_collection: any,
  budget: number,
  genres: [
    {
      id: number,
      name: string,
    }
  ],
  homepage: string,
  id: number,
  imbd_id: string,
  original_language: string,
  original_title: string,
  overview: string,
  popularity: number,
  poster_path: string,
  production_companies: [
    {
      id: number,
      logo_path: string,
      name: string,
      origin_country: string,
    }
  ],
  production_countries: [
    {
      iso_3166_1: string | null,
      name: string,
    }
  ],
  release_date: string,
  revenue: number,
  runtime: number,
  spoken_languages: [
    {
      iso_639_1: string | null,
      name: string,
    }
  ],
  status: string,
  tagline: string,
  title: string,
  video: boolean,
  vote_average: number,
  vote_count: number,
}

export type detailedMovie = {
  adult: boolean,
  backdrop_path: string,
  belongs_to_collection: any,
  budget: number,
  genres: {
    id: number,
    name: string,
  }[],
  homepage: string,
  id: number,
  imbd_id: string,
  original_language: string,
  original_title: string,
  overview: string,
  popularity: number,
  poster_path: string,
  production_companies: {
    id: number,
    logo_path: string,
    name: string,
    origin_country: string,
  }[],
  production_countries: {
    iso_3166_1: string | null,
    name: string,
  }[],
  release_date: string,
  revenue: number,
  runtime: number,
  spoken_languages: {
    iso_639_1: string | null,
    name: string,
  }[],
  status: string,
  tagline: string,
  title: string,
  video: boolean,
  vote_average: number,
  vote_count: number,
  cast: {
    cast_id: number,
    character: string,
    credit_id: number,
    gender: number,
    id: number,
    name: string,
    order: number,
    profile_path: string,
  }[],
  crew: {
    credit_id: string,
    department: string,
    gender: number,
    id: number,
    job: string,
    name: string,
    profile_path: string,
  }[],
  backdrops: {
    aspect_ratio: number,
    file_path: string,
    height: number,
    iso_639_1: string | null,
    vote_average: number,
    vote_count: number,
    width: number,
  }[],
  posters: {
    aspect_ratio: number,
    file_path: string,
    height: number,
    iso_639_1: string | null,
    vote_average: number,
    vote_count: number,
    width: number,
  }[],
  videos: {
    id: string,
    iso_639_1: string | null,
    iso_3166_1: string | null,
    key: string,
    name: string,
    site: string,
    size: number,
    type: string,
  }[],
}

export type basicTV = {
  poster_path: string,
  popularity: number,
  id: number,
  overview: string,
  backdrop_path: string,
  vote_average: number,
  media_type: string,
  first_air_date: string,
  origin_country: string[],
  genre_ids: number[],
  original_language: string,
  vote_count: number,
  name: string,
  original_name: string,
}

export type basicPerson = {
  profile_path: string,
  adult: boolean,
  id: number,
  known_for: (basicMovie & { media_type: 'movie' }) | (basicTV & { media_type: 'tv' })[],
  name: string,
  popularity: number,
}

export type detailedDiscoverOptions = {
  year: {
    specific: boolean,
    year: number | undefined,
  },
  genres: {
    combined: boolean,
    withGenres: number[],
    withoutGenres: number[],
  },
  actors: basicPerson[],
  runtime: {
    longerThan: boolean,
    runtime: number | undefined,
  },
}

export type movieList = {
  page: number,
  total_pages: number,
  total_results: number,
  results: basicMovie[],
}

export type personList = {
  page: number,
  total_pages: number,
  total_results: number,
  results: basicPerson[],
}
