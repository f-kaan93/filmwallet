import { flatten, shuffle, uniqBy } from 'lodash'
import { observable } from 'mobx'
import { AsyncStorage } from 'react-native'
import {
  discoverDetailed,
  getMovie,
  getMovieDetails,
  getRelatedMovies,
  getTopRatedMovies,
  searchActor,
  searchMovie,
} from './themoviedb'
import { basicMovie, basicPerson, detailedDiscoverOptions, detailedMovie, wallet } from './types'

const getItem = () => {
  return AsyncStorage.getItem('wallet')
    .then((res) => {
      if (res) {
        return JSON.parse(res) as wallet
      }
      return [] as wallet
    })
}

const setItem = (wlt: wallet) => {
  return AsyncStorage
    .setItem('wallet', JSON.stringify(wlt))
    .then(getItem)
}

const clearWallet: wallet = [
  { name: 'Favorites', movies: [], type: 'default' },
  { name: 'Watch Later', movies: [], type: 'default' },
  { name: 'Already Watched', movies: [], type: 'default' },
]

class Store {
  @observable searchMovieResults: basicMovie[] = []
  @observable searchActorResults: { results: basicPerson[], currentPage: number } = { results: [], currentPage: 0 }
  @observable details: { title: string, movie: detailedMovie | null } = { title: '', movie: null }
  @observable wallet: wallet = []
  @observable discoverList: { currentPage: number, movies: basicMovie[] } = { currentPage: 0, movies: [] }
  @observable error: { visible: boolean, message: string } = { visible: false, message: ''}

  search(query: string) {
    searchMovie(query)
      .then((res) => {
        this.searchMovieResults = res.results
      })
      .catch((err) => {
        this.showError(err.request._response)
      })
  }
  searchActor(query: string, page = 1) {
    searchActor(query, page).then((res) => {
      this.searchActorResults = {
        currentPage: res.page,
        results: res.page === 1 ? res.results : this.searchActorResults.results.concat(res.results),
      }
    })
  }
  getDetails(id: number) {
    getMovieDetails(id)
      .then((res: detailedMovie) => {
        this.details.movie = res
      })
      .catch((err) => {
        this.showError(err.request._response)
      })
  }
  addMovieToList(listIndex: number, movieId: number) {
    const newWallet = this.wallet
    getMovie(movieId)
      .then((res) => {
        newWallet[listIndex].movies.push(res)
        setItem(newWallet)
          .then((res) => {
            this.wallet = res
          })
      })
      .catch((err) => {
        this.showError(err.request._response)
      })
  }
  addMovieToNewList(listName: string, movieId: number) {
    getMovie(movieId)
      .then((movie) => (
        setItem([
          ...this.wallet,
          {
            name: listName,
            movies: [movie],
            type: 'custom',
          },
        ])))
      .then((res) => {
        this.wallet = res
      })
      .catch((err) => {
        this.showError(err.request._response)
      })
  }
  removeMovieFromList(listIndex: number, movieIndex: number) {
    const newWallet = this.wallet
    newWallet[listIndex].movies.splice(movieIndex, 1)
    setItem(newWallet)
      .then((res) => {
        this.wallet = res
      })
  }
  addListToWallet(listName: string) {
    const newWallet = this.wallet
    newWallet.push({
      movies: [],
      name: listName,
      type: 'custom',
    })
    return setItem(newWallet)
      .then((res) => {
        this.wallet = res
        return this.wallet
      })
  }
  removeListFromWallet(listIndex: number) {
    const newWallet = this.wallet
    newWallet.splice(listIndex, 1)
    setItem(newWallet)
      .then((res) => {
        this.wallet = res
      })
  }
  async basicDiscover() {
    const topRated = (index: number) => {
      return Promise
        // Get 100 movies from top rated
        .all([
          getTopRatedMovies(index),
          getTopRatedMovies(index + 1),
          getTopRatedMovies(index + 2),
          getTopRatedMovies(index + 3),
          getTopRatedMovies(index + 4),
        ])
        // Create a basicMovie[] from response
        .then((res) => {
          const totalPages = res[0].total_pages
          return {
            totalPages,
            movies: flatten(res.map((movieList) => (movieList.results))),
          }
        })
    }

    const related = () => {
      const favoritesList = this.wallet.find((list) => (list.name === 'Favorites'))

      if (favoritesList && favoritesList.movies && favoritesList.movies.length > 0) {
        const favoriteMovies = favoritesList.movies.map((m) => (m.id))
        const promises = favoriteMovies.map((m) => (getRelatedMovies(m)))
        return Promise.all(promises)
          .then((res) => (
            flatten(res.map((movieList) => (movieList.results)))
          ))
          .then((res) => (uniqBy(res, 'id')))
      } else {
        return []
      }
    }

    const filterSeen = (list: basicMovie[]) => {
      let movieIdsToRemove: number[] = []

      const alreadyWatched = this.wallet.find((list) => (list.name === 'Already Watched'))
      const favorites = this.wallet.find((list) => (list.name === 'Favorites'))

      if (alreadyWatched && alreadyWatched.movies && alreadyWatched.movies.length > 0) {
        movieIdsToRemove = movieIdsToRemove.concat(alreadyWatched.movies.map((m) => (m.id)))
      }
      if (favorites && favorites.movies && favorites.movies.length > 0) {
        movieIdsToRemove = movieIdsToRemove.concat(favorites.movies.map((m) => (m.id)))
      }

      if (movieIdsToRemove.length > 0) {
        const filteredList = list.filter((m) => (!movieIdsToRemove.includes(m.id)))
        return filteredList
      }
      return list
    }

    let currentPage = this.discoverList.currentPage

    const topRatedObj = await topRated(currentPage + 1)
    const totalPages = topRatedObj.totalPages
    let movies: basicMovie[] = []
    if (currentPage === 0) {
      const relatedArr = await related()
      movies = shuffle(
        filterSeen(
          uniqBy(
            topRatedObj.movies.concat(relatedArr),
            'id',
          ),
        ),
      )
    } else {
      movies = shuffle(
        filterSeen(topRatedObj.movies),
      )
    }

    currentPage += 5

    while (movies.length < 50 && currentPage < totalPages) {
      movies = shuffle(
        filterSeen((await topRated(currentPage + 1)).movies),
      )
      currentPage += 5
    }

    movies = this.discoverList.movies.concat(movies)

    this.discoverList = {
      currentPage,
      movies,
    }
  }
  detailedDiscover(options: detailedDiscoverOptions, page: number) {
    discoverDetailed(options, page).then((res) => {
      this.discoverList.currentPage = page
      this.discoverList.movies = this.discoverList.movies.concat(res.results)
    })
  }
  showError(message: string) {
    this.error = {
      message,
      visible: true,
    }
  }
  resetWallet() {
    setItem(clearWallet)
      .then((res) => {
        store.wallet = res
      })
  }
  zeroAsyncStorage() {
    AsyncStorage.clear()
      .then(() => {
        return getItem()
      })
      .then((res) => {
        store.wallet = res
      })
  }
  initWallet() {
    getItem()
      .then((res) => {
        if (res.length === 0) {
          return setItem(clearWallet)
        }
        return res
      })
      .then((res) => {
        store.wallet = res
      })
  }
}
const store = new Store()

export default store
