import React from 'react'
import { StatusBar, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { createBottomTabNavigator } from 'react-navigation'
import colors from './colors'
import Error from './components/Error'
import TabBar from './components/TabBar'
import store from './model/store'
import Debug from './pages/Debug'
import Discover from './pages/Discover'
import Search from './pages/Search'
import Wallet from './pages/Wallet'

store.initWallet()

const Tabs = createBottomTabNavigator(
  {
    Discover,
    Search,
    Wallet,
    Debug,
  },
  {
    initialRouteName: 'Discover',
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state
        let iconName = ''
        if (routeName === 'Search') {
          iconName = `ios-search${focused ? '' : '-outline'}`
        } else if (routeName === 'Discover') {
          iconName = `ios-disc${focused ? '' : '-outline'}`
        } else if (routeName === 'Wallet') {
          iconName = `ios-folder-open${focused ? '' : '-outline'}`
        } else if (routeName === 'Debug') {
          iconName = `ios-analytics${focused ? '' : '-outline'}`
        }
        return <Icon name={iconName} size={30} color={tintColor as string} />
      },
    }),
    tabBarComponent: (props) => <TabBar {...props}/>,
    tabBarOptions: {
      activeTintColor: colors.highlight,
      inactiveTintColor: colors.text,
    },
  },
)

export default class Main extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, flexGrow: 1 }}>
        <StatusBar barStyle='light-content' />
        <Tabs />
        <Error />
      </View>
    )
  }
}
