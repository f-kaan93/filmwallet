import { observer } from 'mobx-react'
import React from 'react'
import { ScrollView, View } from 'react-native'
import Swipeout from 'react-native-swipeout'
import Icon from 'react-native-vector-icons/Ionicons'
import colors from '../colors'
import Background from '../components/Background'
import ListItem from '../components/ListItem'
import NavMargin from '../components/NavMargin'
import store from '../model/store'

@observer export default class ListPage extends React.Component<{ navigation: any }, {}> {
  static navigationOptions = ({ navigation }: any) => {
    const { title } = navigation.state.params
    return {
      title,
    }
  }

  setNavigationParams = () => {
    const { params } = this.props.navigation.state
    const { name } = store.wallet[params.index]
    params.title = name
    this.props.navigation.setParams(params)
  }
  componentWillMount() {
    this.setNavigationParams()
  }
  render() {
    const { movies } = store.wallet[this.props.navigation.state.params.index]
    return (
      <View>
        <Background />
        <NavMargin />
        <ScrollView
          alwaysBounceVertical={false}>
          {movies.map((movie, index) => {
            return (
              <Swipeout
                key={movie.id + index}
                backgroundColor='transparent'
                right={[{
                  component: (
                    <View style={{
                      width: 50,
                      height: 40,
                      borderRadius: 15,
                      justifyContent: 'center',
                      alignItems: 'center',
                      alignSelf: 'center',
                      marginTop: 'auto',
                      marginBottom: 'auto',
                      backgroundColor: colors.highlight,
                    }}>
                      <Icon
                        name='ios-trash-outline'
                        color={colors.text}
                        size={35} />
                    </View>
                  ),
                  backgroundColor: 'transparent',
                  onPress: () => {
                    store.removeMovieFromList(
                      this.props.navigation.state.params.index,
                      index,
                    )
                  },
                }]}>
                <ListItem
                  onPress={() => {
                    store.getDetails(movie.id)
                    this.props.navigation.push('Details', { title: movie.title })
                  }}
                  avatar={{ uri: 'http://image.tmdb.org/t/p/w92/' + movie.poster_path }}
                  title={movie.title} />
              </Swipeout>
            )
          })}
          <NavMargin />
        </ScrollView>
      </View>
    )
  }
}
