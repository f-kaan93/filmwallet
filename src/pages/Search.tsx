import { observer } from 'mobx-react'
import React from 'react'
import { ScrollView, View } from 'react-native'
import { createStackNavigator } from 'react-navigation'
import colors from '../colors'
import Background from '../components/Background'
import NavMargin from '../components/NavMargin'
import ListItem from '../components/ListItem'
import SearchBar from '../components/SearchBar'
import store from '../model/store'
import Details from './Details'
import AllCast from './Details/AllCast'

@observer class SearchPage extends React.Component<{ navigation: any }, {}> {
  static navigationOptions = {
    title: 'Search',
  }

  componentWillMount() {
    this.setNavigationParams()
  }

  setNavigationParams = () => {
    const headerTitle = (
      <SearchBar
        onSearch={this.onSearch.bind(this)}
        onCancel={this.onCancel.bind(this)} />
    )
    this.props.navigation.setParams({
      headerTitle,
    })
  }
  onSearch = (input: string) => {
    store.search(input)
  }
  onCancel = () => {
    store.searchMovieResults = []
  }
  render() {
    return (
      <View style={{ flex: 1, flexGrow: 1 }}>
        <Background />
        <NavMargin />
        <SearchBar
          onSearch={this.onSearch}
          onCancel={this.onCancel} />
        {store.searchMovieResults.length === 0 ? null : (
          <ScrollView>
            {
              store.searchMovieResults.map((movie, i) => (
                <ListItem
                  onPress={() => {
                    store.getDetails(movie.id)
                    this.props.navigation.navigate('Details', { title: movie.title, showAddButton: true })
                  }}
                  avatar={{ uri: 'http://image.tmdb.org/t/p/w92/' + movie.poster_path }}
                  key={i}
                  title={movie.title}
                  subtitle={movie.release_date.split('-')[0]} />
              ))
            }
            <NavMargin />
          </ScrollView>
        )}
      </View>
    )
  }
}

const Search = createStackNavigator(
  {
    Search: SearchPage,
    Details,
    AllCast,
  }, {
    navigationOptions: {
      headerTintColor: colors.text,
      headerForceInset: { top: 'always' },
      headerTransparent: true,
    },
  },
)

export default Search
