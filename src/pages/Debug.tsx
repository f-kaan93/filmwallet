import { observer } from 'mobx-react'
import React from 'react'
import { Button, View } from 'react-native'
import Background from '../components/Background'
import NavMargin from '../components/NavMargin'
import store from '../model/store'

@observer class Debug extends React.Component {
  render() {
    return (
      <View>
        <Background />
        <NavMargin />
        <View style={{alignItems: 'center'}}>
          <Button
            title='Reset Wallet'
            onPress={store.resetWallet} />
          <Button
            title='Clear Storage'
            onPress={store.zeroAsyncStorage} />
          <Button
            title='Show Error Message'
            onPress={() => {
              store.error = {
                visible: true,
                message: 'This is a test error message',
              }
            }} />
        </View>
      </View>
    )
  }
}

export default Debug
