import { observer } from 'mobx-react'
import React from 'react'
import { Picker, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, ViewStyle } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { createStackNavigator } from 'react-navigation'
import colors from '../colors'
import Background from '../components/Background'
import Button from '../components/Button'
import ButtonGroup from '../components/ButtonGroup'
import ListItem from '../components/ListItem'
import Modal from '../components/Modal'
import NavMargin from '../components/NavMargin'
import SearchBar from '../components/SearchBar'
import Switcher from '../components/Switcher'
import genres from '../model/genres'
import store from '../model/store'
import { detailedDiscoverOptions } from '../model/types'
import Details from './Details'
import AllCast from './Details/AllCast'
import DiscoverCards from './DiscoverCards'

type state = {
  showDetails: boolean,
  buttonGroupIndex: 0 | 1 | 2 | 3,
  options: detailedDiscoverOptions,
}

@observer class DiscoverHome extends React.Component<{ navigation: any }, {}> {
  static navigationOptions = ({ navigation }: any) => {
    const headerRight = navigation.state.params && navigation.state.params.headerRight
    return {
      headerTitle: 'Discover',
      headerRight,
    }
  }

  state: state = {
    showDetails: false,
    buttonGroupIndex: 0,
    options: {
      actors: [],
      genres: {
        combined: false,
        withGenres: [],
        withoutGenres: [],
      },
      runtime: {
        longerThan: false,
        runtime: undefined,
      },
      year: {
        specific: false,
        year: undefined,
      },
    },
  }
  componentWillMount() {
    this.setNavigationParams()
  }
  setNavigationParams = () => {
    const params = {
      headerRight: (
        <View style={{ marginTop: -20 }}>
          <Button
            title='...'
            fontSize={35}
            onPress={this.showDetails.bind(this)}
            color={colors.text} />
        </View>

      ),
    }
    this.props.navigation.setParams(params)
  }
  showDetails = () => {
    this.setState({
      showDetails: true,
    })
  }
  hideDetails = () => {
    this.setState({
      showDetails: false,
    })
  }
  updateButtonIndex = (buttonGroupIndex: number) => {
    this.setState({ buttonGroupIndex })
  }
  renderGenres = () => {
    const options = this.state.options

    const listItems = genres.map(({ name, id }) => {
      const iconName = options.genres.withGenres.includes(id) ? 'ios-add-circle' :
        options.genres.withoutGenres.includes(id) ? 'ios-remove-circle' : undefined

      const toggleChecked = () => {
        if (options.genres.withGenres.includes(id)) {
          options.genres.withGenres.splice(options.genres.withGenres.indexOf(id), 1)
          options.genres.withoutGenres.push(id)
        } else if (options.genres.withoutGenres.includes(id)) {
          options.genres.withoutGenres.splice(options.genres.withoutGenres.indexOf(id), 1)
        } else {
          options.genres.withGenres.push(id)
        }
        this.setState({ options })
      }
      return (
        <ListItem
          key={id}
          underline
          title={name}
          onPress={toggleChecked}
          leftElement={!iconName ? (
            <View style={styles.genresIconContainer}>
              <View style={styles.genresEmptyCircle}></View>
            </View>
          ) : (
              <View style={styles.genresIconContainer}>
                <Icon name={iconName} color='#888' size={30} />
              </View>
            )} />
      )
    })
    return (
      <View style={styles.scrollViewContainer}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Button
            fontSize={18}
            color={colors.highlight}
            title={options.genres.combined ? 'All' : 'Any'}
            onPress={() => {
              options.genres.combined = !options.genres.combined
              this.setState({ options })
            }} />
          <Text style={{ fontSize: 18, color: colors.text }}>
            of these genres
          </Text>
        </View>
        <Text style={{ color: colors.text, alignSelf: 'center', marginBottom: 10 }}>
          (Double tap to exclude)
        </Text>
        <ScrollView>
          {listItems}
        </ScrollView>
      </View>
    )
  }
  renderYears = () => {
    const options = this.state.options
    const pickerStyle = {
      backgroundColor: colors.elBackground,
      borderRadius: 15,
      marginHorizontal: 10,
      marginTop: 40,
    }
    return (
      <View>
        <Switcher
          buttonNames={['Decades', 'Specific']}
          activeIndex={options.year.specific ? 1 : 0}
          onPress={(i) => {
            if (i === 1) {
              options.year.specific = true
            } else {
              options.year.specific = false
            }
            this.setState({ options })
          }} />
        {(options.year.specific ?
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              keyboardType='numeric'
              returnKeyType='done'
              keyboardAppearance='dark'
              placeholder='Specific year'
              placeholderTextColor={colors.text}
              maxLength={4}
              value={options.year.year ? options.year.year.toString() : ''}
              onChangeText={(input: string) => {
                const year = +input
                if (!isNaN(year)) {
                  options.year.year = parseInt(input, 10)
                  this.setState({ options })
                }
              }} />
          </View>
          :
          <Picker
            style={pickerStyle}
            selectedValue={options.year.year}
            onValueChange={(val) => {
              options.year.year = val
              this.setState({ options })
            }}>
            <Picker.Item color={colors.text} label='All' value={undefined} />
            <Picker.Item color={colors.text} label={'1930\'s'} value={1930} />
            <Picker.Item color={colors.text} label={'1940\'s'} value={1940} />
            <Picker.Item color={colors.text} label={'1950\'s'} value={1950} />
            <Picker.Item color={colors.text} label={'1960\'s'} value={1960} />
            <Picker.Item color={colors.text} label={'1970\'s'} value={1970} />
            <Picker.Item color={colors.text} label={'1980\'s'} value={1980} />
            <Picker.Item color={colors.text} label={'1990\'s'} value={1990} />
            <Picker.Item color={colors.text} label={'2000\'s'} value={2000} />
            <Picker.Item color={colors.text} label={'2010\'s'} value={2010} />
          </Picker>
        )}
      </View>
    )
  }
  renderRuntime = () => {
    const options = this.state.options
    const inputContainer: ViewStyle = StyleSheet.flatten(styles.inputContainer)
    return (
      <View style={{ flexDirection: 'row' }} >
        <View style={{ flexShrink: 0, marginTop: 20, marginLeft: 10 }} >
          <Button
            title={options.runtime.longerThan ? 'Longer than' : 'Shorter than'}
            color={colors.highlight}
            fontSize={16}
            onPress={() => {
              options.runtime.longerThan = !options.runtime.longerThan
              this.setState({ options })
            }} />
        </View>
        <View style={{ ...inputContainer, flex: 1 }}>
          <TextInput
            style={styles.input}
            keyboardType='numeric'
            returnKeyType='done'
            keyboardAppearance='dark'
            maxLength={3}
            value={options.runtime.runtime ? options.runtime.runtime.toString() : ''}
            onChangeText={(input: string) => {
              const runtime = +input
              if (!isNaN(runtime)) {
                options.runtime.runtime = parseInt(input, 10)
                this.setState({ options })
              }
            }} />
          <Text style={styles.label}>minutes</Text>
        </View>
      </View>
    )
  }
  renderActors = () => {
    const { options } = this.state
    const listItems = store.searchActorResults.results.map((actor) => (
      <ListItem
        key={actor.id}
        title={actor.name}
        avatar={{ uri: 'http://image.tmdb.org/t/p/w92/' + actor.profile_path }}
        onPress={() => {
          options.actors.push(actor)
          this.setState({ options })
        }} />
    ))
    return (
      <View style={styles.scrollViewContainer}>
        {options.actors.length === 0 ? null : (
          <View style={{ marginBottom: 20 }}>
            {options.actors.map((actor, index) => (
              <ListItem
                key={index}
                title={actor.name}
                avatar={{ uri: 'http://image.tmdb.org/t/p/w92/' + actor.profile_path }}
                rightElement={
                  <Icon
                    name='ios-close-circle'
                    color={colors.text}
                    onPress={() => {
                      options.actors.splice(index, 1)
                      this.setState({ options })
                    }}
                    size={24} />
                } />
            ))}
          </View>
        )}
        <SearchBar
          onSearch={(input) => {
            store.searchActor(input)
          }}
          onCancel={() => {
            store.searchActorResults = {
              currentPage: 0,
              results: [],
            }
          }}
          placeholder='Search for actors' />
        {listItems.length === 0 ? null : (
          <ScrollView>
            {listItems}
          </ScrollView>
        )}
      </View>
    )
  }

  renderState = () => (
    <ScrollView style={{ height: 200, flexGrow: 0, padding: 5 }}>
      <Text>{JSON.stringify(this.state.options, null, 2)}</Text>
    </ScrollView>
  )
  renderButton = (onPress: () => any) => (
    <TouchableOpacity style={styles.basicButton} onPress={onPress} />
  )
  optionSummary = () => {
    let summary = ''
    const options = this.state.options
    if (options.genres.withGenres.length > 0) {
      const genreNames: string[] = []
      options.genres.withGenres.forEach((selGenre) => {
        genres.filter((dbGenre) => {
          if (selGenre === dbGenre.id) {
            genreNames.push(dbGenre.name)
          }
        })
      })
      summary += 'With Genres: ' + genreNames.join(', ') + '\n'
    }
    if (options.genres.withoutGenres.length > 0) {
      const genreNames: string[] = []
      options.genres.withoutGenres.forEach((selGenre) => {
        genres.filter((dbGenre) => {
          if (selGenre === dbGenre.id) {
            genreNames.push(dbGenre.name)
          }
        })
      })
      summary += 'Without Genres: ' + genreNames.join(', ') + '\n'
    }
    if (options.year.year) {
      summary += (
        options.year.specific ?
          'Specific year: ' + options.year.year :
          options.year.year + '\'s'
      ) + '\n'
    }
    if (options.runtime.runtime) {
      summary += (
        options.runtime.longerThan ?
          'Longer than ' + options.runtime.runtime + ' minutes' :
          'Shorter than ' + options.runtime.runtime + ' minutes'
      ) + '\n'
    }
    if (options.actors.length > 0) {
      summary += (
        'With Actors: ' + options.actors.map((actor) => (actor.name)).join(', ')
      )
    }
    // if (!summary) {
    //   summary = ''
    // } else {
    //   summary = 'Selected Options\n\n' + summary
    // }
    return summary
  }
  renderFilters = () => {
    switch (this.state.buttonGroupIndex) {
      case 0:
        return this.renderGenres()
      case 1:
        return this.renderYears()
      case 2:
        return this.renderRuntime()
      case 3:
        return this.renderActors()
    }
  }
  basicOrDetailed = (): 'basic' | 'detailed' => {
    const { options } = this.state
    if (
      options.actors.length === 0 &&
      options.genres.withGenres.length === 0 &&
      options.genres.withoutGenres.length === 0 &&
      !options.runtime.runtime &&
      !options.year.year
    ) {
      return 'basic'
    }
    return 'detailed'
  }
  resetOptions = () => {
    this.setState({
      options: {
        actors: [],
        genres: {
          combined: false,
          withGenres: [],
          withoutGenres: [],
        },
        runtime: {
          longerThan: false,
          runtime: undefined,
        },
        year: {
          specific: false,
          year: undefined,
        },
      },
    })
  }
  render() {
    const buttonGroup = [
      'Genres',
      'Year',
      'Runtime',
      'Actor',
    ]
    return (
      <View style={styles.scrollViewContainer}>
        <Background />
        <NavMargin />
        <Text style={{ color: colors.text, marginTop: 10 }}>
          {this.optionSummary()}
        </Text>

        <View style={styles.basicButtonContainer}>
          {this.renderButton(() => {
            store.discoverList = { currentPage: 0, movies: [] }
            if (this.basicOrDetailed() === 'basic') {
              store.basicDiscover()
              this.props.navigation.navigate(
                // 'DiscoverResults',
                'DiscoverCards',
                {
                  onNext: () => {
                    store.basicDiscover()
                  },
                },
              )
            } else {
              const options = this.state.options
              store.detailedDiscover(options, 1)
              this.props.navigation.navigate(
                'DiscoverCards',
                {
                  onNext: () => {
                    store.detailedDiscover(options, store.discoverList.currentPage + 1)
                  },
                },
              )
            }

          })}
        </View>
        <Modal
          onRequestClose={() => {
            this.resetOptions()
            this.hideDetails()
          }}
          visible={this.state.showDetails}>
          <View style={styles.headerContainer}>
            <View style={styles.headerLeft}>
              <Button
                title={this.basicOrDetailed() === 'basic' ? 'Back' : 'Discard'}
                onPress={() => {
                  this.resetOptions()
                  this.hideDetails()
                }}
                color={colors.text} />
            </View>
            <View style={styles.headerCenter}>
              <Text style={{
                color: colors.text,
                fontSize: 18,
                fontWeight: '500',
              }}>
                Options
              </Text>
            </View>
            <View style={styles.headerRight}>
              {this.basicOrDetailed() === 'detailed' && (
                <Button
                  title={'Save'}
                  onPress={this.hideDetails}
                  color={colors.text} />
              )}
            </View>
          </View>
          <ButtonGroup
            onPress={this.updateButtonIndex}
            selectedIndex={this.state.buttonGroupIndex}
            buttons={buttonGroup} />
          {this.renderFilters()}
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    flex: 1,
    flexGrow: 1,
  },
  basicButtonContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  basicButton: {
    width: 200,
    height: 200,
    borderRadius: 200,
    backgroundColor: colors.highlight,
    shadowColor: '#000',
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.4,
  },
  genresEmptyCircle: {
    height: 24,
    width: 24,
    marginVertical: 3,
    borderColor: '#888',
    borderWidth: 1,
    borderRadius: 15,
  },
  genresIconContainer: {
    height: 30,
    width: 30,
  },
  switchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
  },
  headerContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingBottom: 10,
  },
  headerLeft: {
    flex: 1,
    alignItems: 'flex-start',
  },
  headerCenter: {
    flex: 2,
    alignItems: 'center',
  },
  headerRight: {
    flex: 1,
    alignItems: 'flex-end',
  },
  inputContainer: {
    marginTop: 20,
    marginHorizontal: 10,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: colors.elBackground,
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  input: {
    color: colors.text,
    padding: 10,
    flexGrow: 1,
  },
  label: {
    color: colors.text,
    padding: 10,
  },
})
const Discover = createStackNavigator(
  {
    DiscoverHome,
    DiscoverCards,
    AllCast,
    Details,
  }, {
    navigationOptions: {
      headerTintColor: colors.text,
      headerForceInset: { top: 'always' },
      headerTransparent: true,
    },
  },
)

export default Discover
