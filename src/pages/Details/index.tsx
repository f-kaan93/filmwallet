import { observer } from 'mobx-react'
import React from 'react'
import {
  Animated,
  Easing,
  ScrollView,
  View,
} from 'react-native'
import colors from '../../colors'
import Background from '../../components/Background'
import Button from '../../components/Button'
import Loading from '../../components/Loading'
import NavMargin from '../../components/NavMargin'
import store from '../../model/store'
import Actors from './Actors'
import AddToWallet from './AddToWallet'
import Info from './Info'
import Poster from './Poster'
import Videos from './Videos'

type props = {
  navigation: any,
}

@observer export default class Details extends React.Component<props, {}> {
  static navigationOptions = ({ navigation }: any) => {
    const { title, headerRight } = navigation.state.params
    return {
      title,
      headerRight,
    }
  }
  state = {
    imgOpacity: new Animated.Value(0),
    textOpacity: new Animated.Value(0),
    modalVisible: false,
    // promptVisible: false,
  }

  componentWillUnmount() {
    store.details.movie = null
    store.details.title = ''
  }

  componentWillMount() {
    this.setNavigationParams()
  }

  setNavigationParams = () => {
    const { params } = this.props.navigation.state
    if (params.showAddButton) {
      params.headerRight = (
        <Button
          color={colors.text}
          title={'Add'}
          onPress={this.openModal.bind(this)} />
      )
    }
    this.props.navigation.setParams(params)
  }
  openModal = () => {
    this.setState({
      modalVisible: true,
    })
  }
  closeModal = () => {
    this.setState({
      modalVisible: false,
    })
  }
  onImgLoaded = () => {
    Animated.stagger(300, [
      Animated.timing(this.state.imgOpacity, {
        duration: 400,
        easing: Easing.in(Easing.cubic),
        toValue: 1,
      }),
      Animated.timing(this.state.textOpacity, {
        duration: 400,
        easing: Easing.in(Easing.cubic),
        toValue: 1,
      }),
    ]).start()
  }
  render() {
    const { imgOpacity, textOpacity } = this.state
    return (
      <View style={{ flexGrow: 1, flex: 1 }}>
        <Background />
        <NavMargin />
        {store.details.movie === null ? (<Loading />) : (
          <ScrollView>
            <Animated.View style={{ opacity: imgOpacity }}>
              <Poster
                url={'http://image.tmdb.org/t/p/original' + store.details.movie.poster_path}
                onLoad={this.onImgLoaded} />
            </Animated.View>
            <Animated.View style={{ padding: 20, opacity: textOpacity }}>
              <Info />
              <Actors onAllCast={() => this.props.navigation.navigate('AllCast')} />
              <Videos />
              <AddToWallet visible={this.state.modalVisible} onClose={this.closeModal} />
            </Animated.View>
            <NavMargin />
          </ScrollView>
        )}
      </View>
    )
  }
}
