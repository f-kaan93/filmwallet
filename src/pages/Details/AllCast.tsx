import { observer } from 'mobx-react'
import React from 'react'
import { ScrollView, View } from 'react-native'
import Background from '../../components/Background'
import ListItem from '../../components/ListItem'
import NavMargin from '../../components/NavMargin'
import store from '../../model/store'

@observer export default class AllCast extends React.Component {
  static navigationOptions = {
    title: 'Cast',
  }
  render() {
    return (
      !store.details.movie ? null : (
        <View style={{flex: 1, flexGrow: 1}}>
          <Background />
          <NavMargin />
          <ScrollView>
            {store.details.movie!.cast.map((person, i) => (
              <ListItem
                avatar={{
                  uri: 'http://image.tmdb.org/t/p/w185/' + person.profile_path,
                }}
                key={i}
                title={person.name}
                subtitle={person.character}
              />
            ))}
            <NavMargin />
          </ScrollView>
        </View>

      )
    )
  }
}
