import React from 'react'
import { Image, ImageStyle, View, ViewStyle } from 'react-native'

type props = {
  url: string,
  onLoad: () => void,
}

export default class Poster extends React.Component<props, {}> {
  render() {
    const wrapper: ViewStyle = {
      alignItems: 'center',
      paddingVertical: 15,
    }
    const imgContainer: ViewStyle = {
      position: 'relative',
      width: 300,
      height: 452,
    }

    const img: ImageStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      borderRadius: 15,
      width: 300,
      height: 452,
    }
    return (
      <View style={wrapper}>
        <View style={imgContainer}>
          <Image
            style={img}
            resizeMode='cover'
            source={{ uri: this.props.url }}
            onLoad={this.props.onLoad}/>
        </View>
      </View>
    )
  }
}
