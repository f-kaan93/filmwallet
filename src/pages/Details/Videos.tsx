import { observer } from 'mobx-react'
import React from 'react'
import { View, ViewStyle, WebView } from 'react-native'
import Swiper from 'react-native-swiper'
import colors from '../../colors'
import store from '../../model/store'

@observer export default class Videos extends React.Component {
  render() {
    const wrapper: ViewStyle = {
      marginVertical: 15,
    }
    const videoContainer: ViewStyle = {
      borderRadius: 15,
      overflow: 'hidden',
      height: 180,
      width: 320,
    }
    return (
      <View style={wrapper}>
        <Swiper
          height={180}
          paginationStyle={{
            bottom: -20,
          }}
          activeDotColor={colors.highlight}>
          {
            store.details.movie!.videos
              .filter((video) => video.site === 'YouTube')
              .map((video) => (
                <View key={video.key} style={videoContainer}>
                  <WebView
                    style={{ flex: 1 }}
                    bounces={false}
                    source={{ uri: `https://www.youtube.com/embed/${video.key}` }} />
                </View>
              ))
          }
        </Swiper>
        {/* To allow space at the bottom for pagination icons */}
        <View style={{ height: 20 }} />
      </View>
    )
  }
}
