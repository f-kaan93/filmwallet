import { observer } from 'mobx-react'
import React from 'react'
import { Image, ImageStyle, Text, TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import colors from '../../colors'
import store from '../../model/store'

type props = {
  onAllCast: () => void,
}

@observer export default class Actors extends React.Component<props, {}> {
  render() {
    const mainContainer: ViewStyle = {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginVertical: 20,
    }
    const actorContainer: ViewStyle = {
      flex: 1,
      alignItems: 'flex-start',
    }
    const actor: ViewStyle = {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 10,
    }
    const img: ImageStyle = {
      height: 48,
      width: 48,
      borderRadius: 24,
      marginRight: 10,
    }
    const txt: TextStyle = {
      color: colors.text,
      textAlign: 'center',
    }
    const linkContainer: ViewStyle = {
      flexDirection: 'row',
      alignItems: 'center',
    }
    const linkTxt: TextStyle = {
      color: colors.highlight,
      marginLeft: 5,
    }
    return store.details.movie && (
      <View style={mainContainer}>
        <View style={actorContainer}>
          {
            store.details.movie.cast
              .slice(0, 3)
              .map((p) => (
                <View key={p.id} style={actor}>
                  <Image style={img} source={{ uri: 'http://image.tmdb.org/t/p/w92/' + p.profile_path }} />
                  <Text style={txt}>{p.name}</Text>
                </View>
              ))
          }
        </View>

        {
          store.details.movie.cast.length > 3 && (
            <TouchableOpacity onPress={this.props.onAllCast}>
              <View style={linkContainer}>
                <Text style={linkTxt}>
                  Show All
                </Text>
                <Icon name='ios-arrow-forward' style={linkTxt} size={25} />
              </View>
            </TouchableOpacity>
          )
        }
      </View>
    )
  }
}
