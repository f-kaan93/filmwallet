import React from 'react'
import {
  Animated,
  Easing,
  ScrollView,
  Text,
  TextInput,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native'
import colors from '../../colors'
import Button from '../../components/Button'
import ListItem from '../../components/ListItem'
import Modal from '../../components/Modal'
import store from '../../model/store'

type props = {
  visible: boolean,
  onClose: () => void,
}

export default class AddToWallet extends React.Component<props, {}> {
  textInputRef!: TextInput
  state = {
    showListView: true,
    listViewOpacity: new Animated.Value(1),
    promptOpacity: new Animated.Value(0),
    input: '',
  }
  showPrompt = () => {

    // Bit of a callback chain, in the order of what's going on:
    // 1- First set listViewOpacity to 0,
    // 2- When it finishes state showListView is set to false so that listView stops rendering
    //    There are buttons that receives user inputs so just hiding it doesn't work
    // 3- When that is done set promptOpacity to 1 so it starts appearing
    // 4- When that is done focus to the input so that user can type without selecting input

    Animated.timing(
      this.state.listViewOpacity,
      {
        duration: 300,
        easing: Easing.out(Easing.cubic),
        toValue: 0,
      },
    ).start(() => {
      this.setState({ showListView: false }, () => {
        Animated.timing(
          this.state.promptOpacity,
          {
            duration: 300,
            easing: Easing.out(Easing.cubic),
            toValue: 1,
          },
        ).start(() => {
          this.textInputRef.focus()
        })
      })
    })
  }
  closePrompt = () => {
    // Reverse of what is going on in showPrompt without last focus step

    Animated.timing(
      this.state.promptOpacity,
      {
        duration: 300,
        easing: Easing.out(Easing.cubic),
        toValue: 0,
      },
    ).start(() => {
      this.setState({ showListView: true }, () => {
        Animated.timing(
          this.state.listViewOpacity,
          {
            duration: 300,
            easing: Easing.out(Easing.cubic),
            toValue: 1,
          },
        ).start()
      })
    })
  }
  onSubmit = () => {
    store.addMovieToNewList(this.state.input, store.details.movie!.id)
    this.props.onClose()
  }
  render() {
    const listViewWrapper = {
      opacity: this.state.listViewOpacity,
    }
    const propmptWrapper = {
      opacity: this.state.promptOpacity,
    }
    const headerContainer: ViewStyle = {
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'space-between',
      flexShrink: 0,
    }
    const headerLeft: ViewStyle = {
      flex: 1,
      alignItems: 'flex-start',
    }
    const headerCenter: ViewStyle = {
      flex: 2,
      alignItems: 'center',
    }
    const headerRight: ViewStyle = {
      flex: 1,
      alignItems: 'flex-end',
    }
    const headerTitle: TextStyle = {
      fontSize: 17,
      fontWeight: 'bold',
      color: colors.text,
    }
    const inputContainer: ViewStyle = {
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
    }
    const input: TextStyle = {
      backgroundColor: colors.elBackground,
      padding: 10,
      borderRadius: 20,
      marginBottom: 20,
      alignSelf: 'stretch',
      color: colors.text,
    }
    return (
      <Modal
        onRequestClose={this.props.onClose}
        visible={this.props.visible}>
        {this.state.showListView ? (
          <Animated.View style={listViewWrapper}>
            <View style={headerContainer}>
              <View style={headerLeft}>
                <Button
                  color={colors.text}
                  title='New List'
                  onPress={this.showPrompt} />
              </View>
              <View style={headerCenter}>
                <Text style={headerTitle}>Add to list</Text>
              </View>
              <View style={headerRight}>
                <Button
                  color={colors.text}
                  title={'Close'}
                  onPress={this.props.onClose} />
              </View>
            </View>
            <ScrollView style={{ flexGrow: 1 }}>
              {store.wallet.map((list, index) => (
                <ListItem
                  underline
                  onPress={() => {
                    store.addMovieToList(index, store.details.movie!.id)
                    this.props.onClose()
                  }}
                  key={index}
                  title={list.name} />
              ))}
            </ScrollView>
          </Animated.View>

        ) : (

            <Animated.View style={propmptWrapper}>
              <View style={headerContainer}>
                <View style={headerLeft}>
                  <Button
                    color={colors.text}
                    title='Back'
                    onPress={this.closePrompt} />
                </View>
                <View style={headerCenter}>
                  <Text style={headerTitle}>New List</Text>
                </View>
                <View style={headerRight}>
                  <Button
                    color={colors.text}
                    title={'Close'}
                    onPress={this.props.onClose} />
                </View>
              </View>
              <View style={{ flexGrow: 1 }}>
                <View style={inputContainer}>
                  <TextInput
                    placeholder='List Name'
                    placeholderTextColor={colors.text}
                    keyboardAppearance='dark'
                    ref={(ref: TextInput) => { this.textInputRef = ref }}
                    style={input}
                    onChangeText={(input) => this.setState({input})}
                    value={this.state.input} />
                  <Button
                    color={colors.highlight}
                    title='Create'
                    onPress={this.onSubmit} />
                </View>
              </View>
            </Animated.View>
          )}
      </Modal>
    )
  }
}
