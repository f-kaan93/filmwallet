import { observer } from 'mobx-react'
import React from 'react'
import { Text, TextStyle, View, ViewStyle } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import colors from '../../colors'
import store from '../../model/store'
import { detailedMovie } from '../../model/types'

@observer export default class Info extends React.Component {
  date = (movie: detailedMovie) => {
    if (!movie.release_date) {
      return 'unknown'
    }
    let date = movie.release_date.split('-').reverse().join('-')
    if (movie.status !== 'Released') {
      date += ' (' + movie.status + ')'
    }
    return date
  }
  runtime = (movie: detailedMovie) => {
    if (!movie.runtime) {
      return 'unknown'
    }
    const hour = Math.floor(movie.runtime / 60)
    const minute = movie.runtime % 60
    const runtime = hour.toString() + ':' + ('00' + minute).substr(-2, 2)
    return runtime
  }
  director = (movie: detailedMovie) => {
    if (!movie.crew) {
      return 'Director: unknown'
    }
    const directors = movie.crew
      .filter((p) => p.job === 'Director')
      .map((p) => p.name)
    if (directors.length === 0) {
      return 'Director: unknown'
    }
    const header = directors.length > 1 ? 'Directors: ' : 'Director: '
    return header + directors.join(', ')
  }
  render() {
    const titleContainer: ViewStyle = {
      alignItems: 'center',
      paddingVertical: 25,
    }
    const container: ViewStyle = {
      paddingVertical: 15,
      flexDirection: 'row',
      alignItems: 'center',
    }
    const title: TextStyle = {
      fontWeight: '500',
      fontSize: 20,
      textAlign: 'center',
    }
    const text: TextStyle = {
      color: colors.text,
    }
    const iconProps = {
      size: 30,
      color: colors.text,
      style: {
        marginRight: 10,
      },
    }
    return store.details.movie && (
      <View>
        <View style={titleContainer}>
          <Text style={{ ...title, ...text }}>
            {store.details.movie.title}
          </Text>
        </View>
        <View style={container}>
          <Text style={text}>
            {this.director(store.details.movie)}
          </Text>
        </View>
        <View style={container}>
          <Icon name={'ios-calendar-outline'} {...iconProps} />
          <Text style={text}>{this.date(store.details.movie)}</Text>
        </View>
        <View style={container}>
          <Icon name={'ios-clock-outline'} {...iconProps} />
          <Text style={text}>{this.runtime(store.details.movie)}</Text>
        </View>
        <View style={container}>
          <Text style={text}>
            {store.details.movie.overview}
          </Text>
        </View>
      </View>
    )
  }
}
