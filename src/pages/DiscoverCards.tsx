import { observer } from 'mobx-react'
import React from 'react'
import { Dimensions, Image, View } from 'react-native'
import colors from '../colors'
import Background from '../components/Background'
import Cards from '../components/Cards'
import Loading from '../components/Loading'
import NavMargin from '../components/NavMargin'
import store from '../model/store'
import { basicMovie } from '../model/types'

@observer export default class DiscoverCards extends React.Component<{ navigation: any }, {}> {
  componentWillUnmount() {
    store.discoverList = { currentPage: 0, movies: [] }
  }
  renderCard = (movie: basicMovie, index: number) => {
    const width = Dimensions.get('window').width * 0.8
    const height = width * 1.2 // approximate movie poster aspect ratio
    return (
      <Image
        key={movie.id}
        source={{ uri: 'http://image.tmdb.org/t/p/w500/' + movie.poster_path }}
        style={{ borderRadius: 10, width, height }} />
    )
  }
  render() {
    // TODO
    // Add loading anim without length check
    return (
      <View style={{flex: 1}}>
        <Background />
        <NavMargin />
        {store.discoverList.movies.length === 0 ? <Loading /> : (
          <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 50,
            flex: 1,
          }}>
            <Cards
              messageStyle={{
                color: colors.text,
                fontSize: 20,
              }}
              onSwipeLeft={(i) => { store.addMovieToList(2, store.discoverList.movies[i].id) }}
              onSwipeLeftMessage='Already Watched'
              onSwipeRight={() => null}
              onSwipeRightMessage={'Skip'}
              onSwipeUp={(i) => { store.addMovieToList(0, store.discoverList.movies[i].id) }}
              onSwipeUpMessage={'Favorite'}
              onIndex={(index) => {
                if (index === store.discoverList.movies.length - 5) {
                  this.props.navigation.getParam('onNext')()
                }
              }}
              onPress={(i) => {
                const movie = store.discoverList.movies[i]
                store.getDetails(movie.id)
                this.props.navigation.push('Details', { title: movie.title, showAddButton: true })
              }}>
              {store.discoverList.movies.map((movie, index) => (
                this.renderCard(movie, index)
              ))}
            </Cards>
          </View>
        )}
      </View>
    )
  }
}
