import { observer } from 'mobx-react'
import React from 'react'
import { ScrollView, View } from 'react-native'
import Swipeout from 'react-native-swipeout'
import Icon from 'react-native-vector-icons/Ionicons'
import { createStackNavigator } from 'react-navigation'
import colors from '../colors'
import Background from '../components/Background'
import ListItem from '../components/ListItem'
import NavMargin from '../components/NavMargin'
import Prompt from '../components/Prompt'
import store from '../model/store'
import Details from './Details'
import AllCast from './Details/AllCast'
import ListPage from './List'

@observer class WalletPage extends React.Component<{ navigation: any }, {}> {

  static navigationOptions = {
    title: 'Wallet',
  }

  state = {
    showPrompt: false,
    listToDelete: {
      name: '',
      index: -1,
    },
  }

  render() {
    return (
      <View>
        <Background />
        <ScrollView
          style={{ height: '100%' }}
          alwaysBounceVertical={false}>
          <NavMargin />
          {store.wallet.map((list, index) => (
            <Swipeout
              key={index}
              backgroundColor='transparent'
              disabled={list.type === 'default' ? true : false}
              right={[{
                component: (
                  <View style={{
                    width: 50,
                    height: 40,
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                    marginTop: 'auto',
                    marginBottom: 'auto',
                    backgroundColor: colors.highlight,
                  }}>
                    <Icon
                      name='ios-trash-outline'
                      color={colors.text}
                      size={35} />
                  </View>

                ),
                backgroundColor: 'transparent',
                onPress: () => {
                  this.setState({
                    listToDelete: { name: list.name, index },
                    showPrompt: true,
                  })
                },
              }]}>
              <ListItem
                underline
                onPress={() => {
                  this.props.navigation.navigate('List', { index })
                }}
                title={list.name}>
              </ListItem>
            </Swipeout>
          ))}
          <Prompt
            visible={this.state.showPrompt}
            title={`Do you really want to delete the list ${this.state.listToDelete.name}?`}
            message={'You cannot undo this process'}
            cancelText='No'
            submitText='Yes'
            onCancel={() => { this.setState({ showPrompt: false }) }}
            onSubmit={() => {
              this.setState({ showPrompt: false })
              store.removeListFromWallet(this.state.listToDelete.index)
            }} />
          <NavMargin />
        </ScrollView>
      </View>
    )
  }
}

const Wallet = createStackNavigator(
  {
    Wallet: WalletPage,
    List: ListPage,
    Details,
    AllCast,
  }, {
    navigationOptions: {
      headerTintColor: colors.text,
      headerForceInset: { top: 'always' },
      headerTransparent: true,
    },
  },
)
export default Wallet
