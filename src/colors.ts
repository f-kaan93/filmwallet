const colors = {
  appBackground: ['#414141', '#571212'],
  elBackground: '#00000080',  // Background color for elements (list, tab etc.)
  popUpBackground: '#231A1A',
  highlight: '#D60000',       // Main color
  text: '#E6E6E6',
}

export default colors
